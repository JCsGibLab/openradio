/*
 * Copyright (C) 2020  Wproject - Aitzol Berasategi
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * OpenRadio is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.9
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.2
import QtQuick.Controls.Material 2.1
import QtQuick.Controls.Universal 2.1

import Ubuntu.Components 1.3 as Ubuntu

ToolBar {
    id: multiPageHeader
    width: root.width

    background: Rectangle {
        implicitWidth: parent.width
        implicitHeight: units.gu(6)
        color: theme.palette.normal.background

        Rectangle {
            width: parent.width
            height: 1
            anchors.bottom: parent.bottom
            color: "transparent"
            border.color: theme.palette.normal.overlaySecondaryText
        }
        
    }

    RowLayout {
        spacing: 20
        anchors.fill: parent

        ToolButton {
            id: backBtn
            contentItem: Ubuntu.Icon {
                name: "back"
            }

            background: Rectangle {
                implicitWidth: units.gu(4)
                implicitHeight: units.gu(4)
                opacity: enabled ? 1 : 0.3
                color: theme.palette.normal.overlaySecondaryText
                //color: Qt.darker("#33333333", backBtn.enabled && (backBtn.checked || backBtn.highlighted) ? 1.5 : 1.0)
                radius: units.gu(0.6)
                visible: backBtn.down || (backBtn.enabled && (backBtn.checked || backBtn.highlighted))
            }

            onClicked: {
                favoritesView = true;
                listModel = favorites;
                currentHeader = mainHeader
                stackView.pop(page1)
            }
        }

        Label {
            id: titleLabel
            text: headerTitle
            color: theme.palette.normal.baseText
            elide: Label.ElideRight
            horizontalAlignment: Qt.AlignLeft
            verticalAlignment: Qt.AlignVCenter
            Layout.fillWidth: true
        }

    }
}