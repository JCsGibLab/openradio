import QtQuick 2.9
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.2
import Ubuntu.Components.Themes 1.3
import Ubuntu.Components 1.3 as Ubuntu
 

    Drawer {
        id: drawer

        width: Math.min(root.width, root.height) / 3 * 2
        height: root.height
        interactive: stackView.depth === 1
        dragMargin: 0

        background: Rectangle {
            width: parent.width
            height: parent.height
            //color: theme.palette.normal.foreground
            color: theme.palette.normal.background
        }

        ListView {
            id: listView
            focus: true
            //currentIndex: -1
            anchors.fill: parent
            //highlight: Rectangle { width: 180; height: 40; color: theme.palette.normal.focus}

            delegate: ItemDelegate {
                width: parent.width
                Text {
                    text: model.title
                    color: theme.palette.normal.backgroundText
                    anchors.verticalCenter: parent.verticalCenter
                    horizontalAlignment: Text.AlignLeft
                    leftPadding: units.gu(2)
                }
                //highlighted: ListView.isCurrentItem
                background: Rectangle {
                    width: parent.width
                    height: parent.height
                    color: parent.hovered ? theme.palette.normal.foreground : theme.palette.normal.background

                    Rectangle {
                        id: divider
                        //y: 0
                        height: 1
                        width: parent.width
                        anchors.bottom: parent.bottom
                        color: theme.palette.normal.overlaySecondaryText
                    }
                }

                Ubuntu.Icon{
                    name: model.name
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.right: parent.right
                    anchors.rightMargin: units.gu(3)
                    width: units.gu(3)
                    height: units.gu(3)
                    color: theme.palette.normal.foregroundText
                }
                onClicked: {
                    listView.currentIndex = index
                    if(view){
                        switch(model.view){
                            case "settings":    headerTitle = i18n.tr("Settings")
                                                currentHeader = multiPageHeader
                                                stackView.push(settingsPage);
                                                break;
                            case "about":       headerTitle = i18n.tr("About")
                                                currentHeader = multiPageHeader
                                                stackView.push(aboutPage);
                                                break;
                        }
                        drawer.close()

                    }else if(dialog){
                        switch(model.dialog){
                            //case "sleep":       sleepDialog.open();
                            //case "sleep":       settings.infoAlert ? notificationDialog.open() : sleepDialog.open()


                            case "sleep":       if(settings.infoAlert){
                                                    notificationDialog.params = {
                                                        title: i18n.tr("Info"),
                                                        text: i18n.tr("For a correct functioning of the <strong>sleep</strong> timer, it may be necessary to prevent the app suspension. For that you can use the <strong>UT Tweak Tool</strong> application."),
                                                        sleep: true
                                                        //buttonColor:theme.palette.normal.porcelain,
                                                    }
                                                    notificationDialog.open();
                                                }else{
                                                    sleepDialog.open();
                                                }
                                                break;

                        }
                        drawer.close()

                    }else{
                        drawer.close()
                    }

                }
            }



model: ListModel {
        id: menuModel
        Component.onCompleted: {
            menuModel.append({ title: i18n.tr("Sleep"), dialog: "sleep", name: "timer" })
            menuModel.append({ title: i18n.tr("Settings"), view: "settings", name: "settings" })
            menuModel.append({ title: i18n.tr("About"), view: "about", name: "info"})
      }
}


/*
            model: ListModel {
            
                ListElement { title: i18n.tr("Sleep"); dialog: "sleep"; name: "timer" }
                ListElement { title: i18n.tr("Settings"); view: "settings"; name: "settings" }
                ListElement { title: i18n.tr("About"); view: "about"; name: "info"}

            }
*/
            ScrollIndicator.vertical: ScrollIndicator { }
        }
    }