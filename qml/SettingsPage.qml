/*
 * Copyright (C) 2020  Wproject - Aitzol Berasategi
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * OpenRadio is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
import QtQuick 2.9
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.2
import Ubuntu.Components 1.3
import "components"
import "countries.js" as Countries

Page {
    id: settingsPage
    visible: false
    header: PageHeader{visible: false}

    Flickable {
        id: settingsPlugin
        anchors.fill: parent
        contentHeight: _settingsColumn.height + units.gu(2)

        ColumnLayout{
            id: _settingsColumn
            spacing: 20

            anchors {
                top: parent.top
                left: parent.left
                right: parent.right
            }

            Label {
                text: i18n.tr("Theme")
                font.bold: true
                wrapMode: Label.Wrap
                horizontalAlignment: Qt.AlignLeft
                Layout.leftMargin : units.gu(3)
                Layout.topMargin : units.gu(3)
            }
            Label {
                text: i18n.tr("Select UI theme.")
                elide: Text.ElideRight
                wrapMode: Label.Wrap
                horizontalAlignment: Qt.AlignLeft
                maximumLineCount: 2
                Layout.leftMargin : units.gu(3)
                Layout.bottomMargin : units.gu(1)
                Layout.fillWidth: true
                Layout.fillHeight: true
                Layout.maximumHeight: implicitHeight
            }

            ComboBox {
                id: themeCombo
                textRole: "key"
                anchors.horizontalCenter: parent.horizontalCenter
                model: [{key: "System", value: ""}, {key: "Ambiance", value: "Ubuntu.Components.Themes.Ambiance"}, {key: "Suru-Dark", value: "Ubuntu.Components.Themes.SuruDark"}]
                //currentIndex: getCurrentIndex()

                delegate: ItemDelegate {
                    width: themeCombo.width
                    contentItem: Text {
                        text: modelData.key
                        color: theme.palette.normal.backgroundText
                        font: themeCombo.font
                        elide: Text.ElideRight
                        verticalAlignment: Text.AlignVCenter
                    }
                    
                    background: Rectangle{
                        id: hl
                        width: parent.width
                        //color: highlighted ? theme.palette.normal.focus : theme.palette.normal.background
                        color: highlighted ? theme.palette.normal.focus : "transparent"
                        radius: index == 0 || index == themeCombo.model.length-1  ? units.gu(0.6) : 0
                    }
                    
                    highlighted: themeCombo.highlightedIndex === index

                }
    
                contentItem: Text {
                    leftPadding: 0
                    rightPadding: themeCombo.indicator.width + themeCombo.spacing

                    text: themeCombo.displayText
                    font: themeCombo.font
                    color: themeCombo.pressed ? theme.palette.normal.field : theme.palette.normal.fieldText
                    verticalAlignment: Text.AlignVCenter
                    elide: Text.ElideRight
                }
                background: Rectangle {
                    implicitWidth: units.gu(20)
                    implicitHeight: units.gu(4)

                    //border.width: themeCombo.visualFocus ? 2 : 1
                    border.width: themeCombo.visualFocus ? 2 : 1

                    color: themeCombo.hovered ? theme.palette.normal.foreground : theme.palette.normal.field
                    border.color: themeCombo.activeFocus ? theme.palette.normal.focus : theme.palette.normal.overlaySecondaryText
                    radius: units.gu(0.6)

                }

                popup: Popup {
                    y: themeCombo.height - 1
                    width: themeCombo.width
                    implicitHeight: contentItem.implicitHeight
                    padding: 0

                    contentItem: ListView {
                        clip: true
                        implicitHeight: contentHeight
                        model: themeCombo.popup.visible ? themeCombo.delegateModel : null
                        currentIndex: themeCombo.highlightedIndex
                        //highlight: Rectangle { color: theme.palette.normal.focus;}
                        ScrollIndicator.vertical:ScrollIndicator {
                                                    size: 0.3
                                                    position: 0.2
                                                    active: true
                                                    orientation: Qt.Vertical

                                                    contentItem: Rectangle {
                                                        implicitWidth: units.gu(0.4)
                                                        implicitHeight: 100
                                                        color: theme.palette.normal.focus
                                                    }
                                                }
                    }

                    background: Rectangle {
                        color: theme.palette.normal.background
                        border.color: theme.palette.normal.focus
                        radius: units.gu(0.6)
                    }
                }

                indicator: Canvas {
                    id: canvas
                    x: themeCombo.width - width - themeCombo.rightPadding
                    y: themeCombo.topPadding + (themeCombo.availableHeight - height) / 2
                    width: units.gu(2)
                    height: units.gu(0.6)
                    contextType: "2d"

                    Connections {
                        target: themeCombo
                        function onPressedChanged() { canvas.requestPaint(); }
                    }

                    onPaint: {
                        context.reset();
                        context.moveTo(0, 0);
                        context.lineTo(width, 0);
                        context.lineTo(width / 2, height);
                        context.closePath();
                        //context.fillStyle = themeCombo.pressed ? "#17a81a" : "#21be2b";
                        context.fillStyle = theme.palette.normal.focus
                        context.fill();
                    }
                }

                onActivated: {
                    settings.theme = model[currentIndex].value;
                    canvas.requestPaint();
                    canvas2.requestPaint();
                }

            }

            /*************/
            Label {
                text: i18n.tr("Country")
                font.bold: true
                wrapMode: Label.Wrap
                horizontalAlignment: Qt.AlignLeft
                Layout.leftMargin : units.gu(3)
                Layout.topMargin : units.gu(3)
            }
            Label {
                text: i18n.tr("Select the country from which stations will be collected.")
                elide: Text.ElideRight
                wrapMode: Label.Wrap
                horizontalAlignment: Qt.AlignLeft
                maximumLineCount: 2
                Layout.leftMargin : units.gu(3)
                Layout.bottomMargin : units.gu(1)
                Layout.fillWidth: true
                Layout.fillHeight: true
                Layout.maximumHeight: implicitHeight
            }

            ComboBox {
                id: countryCombo
                textRole: "key"
                anchors.horizontalCenter: parent.horizontalCenter
                //model: [{key: "France", value: "FR", flag: "fr.png"}, {key: "Netherland", value: "NL", flag: "nl.png"}, {key: "Italy", value: "IT", flag: "it.png"}, {key: "Spain", value: "ES", flag: "es.png"}]
                model: Countries.list
            
                delegate: ItemDelegate {
                    width: countryCombo.width
                    contentItem: Text {
                        text: modelData.key
                        color: theme.palette.normal.backgroundText
                        font: countryCombo.font
                        elide: Text.ElideRight
                        verticalAlignment: Text.AlignVCenter
                        leftPadding: units.gu(1.6)
                    }

                    Image{
                        source:"images/flags/"+modelData.flag
                        anchors.verticalCenter: parent.verticalCenter
                        anchors.left: parent.left
                        anchors.leftMargin: units.gu(1)
                        fillMode: Image.PreserveAspectFit
                        width: units.gu(2)
                        height: units.gu(2)
                        visible: true
                        asynchronous: true
                    }

                    background: Rectangle{
                        id: hl
                        width: parent.width
                        //color: highlighted ? theme.palette.normal.focus : theme.palette.normal.background
                        color: highlighted ? theme.palette.normal.focus : "transparent"
                        radius: index == 0 || index == countryCombo.model.length-1  ? units.gu(0.6) : 0
                    }
                    
                    highlighted: countryCombo.highlightedIndex === index

                }
    
                contentItem: Text {
                    leftPadding: units.gu(1.6)
                    rightPadding: countryCombo.indicator.width + countryCombo.spacing
                    text: countryCombo.displayText
                    font: countryCombo.font
                    color: countryCombo.pressed ? theme.palette.normal.field : theme.palette.normal.fieldText
                    verticalAlignment: Text.AlignVCenter
                    elide: Text.ElideRight
                }

                Image{
                        source:{
                            for (var i = 0; i < countryCombo.model.length; i++) {
                                if (countryCombo.model[i].key == countryCombo.displayText){
                                    return("images/flags/"+countryCombo.model[i].flag)
                                }
                            }
                        }
                        anchors.verticalCenter: parent.verticalCenter
                        anchors.left: parent.left
                        anchors.leftMargin: units.gu(1)
                        fillMode: Image.PreserveAspectFit
                        width: units.gu(2)
                        height: units.gu(2)
                        visible: true
                        asynchronous: true
                }


                background: Rectangle {
                    implicitWidth: units.gu(20)
                    implicitHeight: units.gu(4)

                    //border.width: countryCombo.visualFocus ? 2 : 1
                    border.width: countryCombo.visualFocus ? 2 : 1

                    color: countryCombo.hovered ? theme.palette.normal.foreground : theme.palette.normal.field
                    border.color: countryCombo.activeFocus ? theme.palette.normal.focus : theme.palette.normal.overlaySecondaryText
                    radius: units.gu(0.6)

                }

                popup: Popup {
                    y: countryCombo.height - 1
                    width: countryCombo.width
                    implicitHeight: contentItem.implicitHeight
                    padding: 0

                    contentItem: ListView {
                        clip: true
                        implicitHeight: contentHeight
                        model: countryCombo.popup.visible ? countryCombo.delegateModel : null
                        currentIndex: countryCombo.highlightedIndex
                        //highlight: Rectangle { color: theme.palette.normal.focus;}
                        ScrollIndicator.vertical:ScrollIndicator {
                                                    size: 0.3
                                                    position: 0.2
                                                    active: true
                                                    orientation: Qt.Vertical

                                                    contentItem: Rectangle {
                                                        implicitWidth: units.gu(0.4)
                                                        implicitHeight: 100
                                                        color: theme.palette.normal.focus
                                                    }
                                                }
                    }

                    background: Rectangle {
                        color: theme.palette.normal.background
                        border.color: theme.palette.normal.focus
                        radius: units.gu(0.6)
                    }
                }

                indicator: Canvas {
                    id: canvas2
                    x: countryCombo.width - width - countryCombo.rightPadding
                    y: countryCombo.topPadding + (countryCombo.availableHeight - height) / 2
                    width: units.gu(2)
                    height: units.gu(0.6)
                    contextType: "2d"

                    Connections {
                        target: countryCombo
                        function onPressedChanged() { canvas2.requestPaint(); }
                    }

                    onPaint: {
                        context.reset();
                        context.moveTo(0, 0);
                        context.lineTo(width, 0);
                        context.lineTo(width / 2, height);
                        context.closePath();
                        //context.fillStyle = countryCombo.pressed ? "#17a81a" : "#21be2b";
                        context.fillStyle = theme.palette.normal.focus
                        context.fill();
                    }
                }

                onActivated: {
                    //settings.theme = model[currentIndex].value;
                    console.log('Country: ' + currentText + ', Code: ' + model[currentIndex].value)
                    settings.country = model[currentIndex].value;
                    //create new database
                    python.call('app.main.change_country',[settings.country], function(result){
                        console.log('db stations created!!')
                        //reload stations database
                        python.call('app.main.get_stations', [], function(res){
                            stations = res;

                            notificationDialog.params = {
                                title: i18n.tr("Country selection"),
                                text: i18n.tr("New database has been created!"),
                                buttonColor: UbuntuColors.green,
                                //buttonColor:theme.palette.normal.porcelain,
                            }
                            notificationDialog.open();
                        });
                    });
                }

            }


        }

    }
    
    Component.onCompleted: {
        
        for(var i=0;i<themeCombo.model.length;i++){
            var val = themeCombo.model[i].value;
            if(val === settings.theme){
                console.log(i+':'+val)
                themeCombo.currentIndex = i;  
            }
        }

        for(var i=0;i<countryCombo.model.length;i++){
            var val = countryCombo.model[i].value;
            if(val === settings.country){
                console.log(i+':'+val)
                countryCombo.currentIndex = i;  
            }
        }

    }
    
}
